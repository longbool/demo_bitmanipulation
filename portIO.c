/* 
 * Demonstration der Eingabe und Ausgabe einzelner Bytes und Bits
 * auf Hardware-Ports, fuer die Mikrocontroller-Familie 8051.
 *
 * Die erste Datei dieses Satzes von Dateien ist cGrundlagen.c.
 *
 * Jan Bredereke, 2020
 */

/* Bei den Mikrocontrollern der 8051-Familie greift man auf alle
 * Peripherie zu, indem man bestimmte spezielle Speicherbereiche
 * liest und schreibt. Der Speicheradressraum der 8051-Familie ist
 * in ziemlich viele verschiedene Bereiche aufgeteilt, was wir an
 * dieser Stelle aber nicht weiter erklaeren. Jedenfalls liegen in
 * einem bestimmten Teilbereich des Speicheradressraums die
 * "Special-Function-Register".
 *
 * Die Mikrocontroller der 8051-Familie haben mehrere "Ports". Jeder
 * Port ist ein Buendel von acht einzelnen digitalen Leitungen.
 * Ueber jede dieser Leitungen kann ein Bit eingelesen oder
 * ausgegeben werden. Daher erlaubt ein Port, ein einzelnes Byte
 * einzulesen oder auszugeben. Es ist allerdings auch moeglich, die
 * Bits eines Ports getrennt zu verwenden, auch in verschiedene
 * Richtungen.
 *
 * Bei dem Mikrocontroller C515C in unserem Labor haben wir die
 * folgenden Ports zur freien Verfuegung, vergleiche auch das Foto
 * des Boards in Aulis:
 * - Port 5 ist auf acht Buchsen an der rechten Seite des Boards
 *   herausgefuehrt. Normalerweise steckt dort eine Platine mit acht
 *   LEDs. Bei Ausgabe einer 0 leuchtet die zugehoerige LED.
 * - Port 4 ist ebenfalls auf acht Buchsen links daneben
 *   herausgefuehrt. Allerdings werden die Bits 6 und 7 bereits
 *   fuer den CAN-Bus verwendet. Zumindest Bit 7 kann nicht anders
 *   verwendet werden.
 * - Port 1 ist auf der linken Seite herausgefuehrt. Mit einem
 *   entsprechend gekennzeichneten Jumper kann man waehlen, ob man
 *   die acht Bits fuer eine Eingabe oder fuer eine Ausgabe verwenden
 *   moechte. Bei einer Ausgabe werden die acht LEDs neben dem Jumper
 *   verwendet. Bei Ausgabe einer 1 leuchtet die zugehoerige LED.
 *   Fuer die Eingabe werden die acht Tipptaster dort verwendet.
 *   Neben jedem Taster ist eine weitere LED. Einmal Tippen schaltet
 *   sie ein (bedeutet 1), nocheinmal Tippen schaltet sie aus
 *   (bedeutet 0).
 * - Von Port 3 sind nur einige Bits herausgefuehrt, siehe das Foto
 *   in Aulis. Die anderen Bits haben nicht aenderbare
 *   Sonderfunktionen, z.B. fuer die serielle Schnittstelle.
 *
 * Das Umschalten zwischen Eingaberichtung und Ausgaberichtung
 * erfolgt bei Mikrocontrollern der 8051-Familie recht
 * ungewoehnlich, im Vergleich zu anderen Mikrocontrollern. Nach
 * einem Reset liegt jeder Anschlusspin erst einmal auf 1. Dies wird
 * durch einen Pull-Up-Widerstand erreicht. Daher darf man diesen
 * Anschlusspin von aussen einfach mit Masse verbinden. Wenn die
 * Software den Wert von diesem Anschlusspin einliest, bekommt sie
 * den entsprechenden logischen Wert. Wenn die Software auf einem
 * solchen Anschlusspin eine 0 ausgibt, dann verbindet ein
 * Ausgangstransistor der Anschlusspin mit Masse. Dies kann eine
 * aeussere Schaltung als Eingabesignal verwenden. Gibt man wieder
 * eine 1 aus, ist alles wie vorher. Zu jedem Anschlusspin gibt es
 * ein Ausgangs-Flipflop, dass den zuletzt durch die Software dort
 * ausgegebenen Wert zwischenspeichert. Man darf keinesfalls einen
 * Anschlusspin von aussen direkt mit +5V verbinden und danach den
 * Mikrocontroller eine 0 ausgeben lassen. Dies zerstoert recht
 * zuverlaessig den Mikrocontroller durch einen Kurzschluss.
 *
 * Nun sollte klar, sein, warum die LEDs an Port 5 bei dem Wert 0
 * leuchten. Sie sind ueber einen Schutzwiderstand einerseits mit +5 V 
 * und andererseits mit je einem Anschlusspin verbunden, das auf 0
 * geschaltet werden kann. (Bei Port 1 sind zusaetzliche Gatter
 * zwischengeschaltet, die fuer eine Invertierung sorgen.)
 *
 * Um aus der Sprache C heraus auf Special-Function-Register und
 * damit u.a. auf Ports zugreifen zu koennen, muss man ihnen Namen
 * geben. Fuer den byteweisen Zugriff geschieht dies mit dem
 * Konstrukt "sfr", fuer den bitweisen Zugriff mit dem Konstrukt
 * "sbit". Beide Konstrukte sind Erweiterungen der Sprache C durch
 * die Firma Keil. Diese Benennungskonstrukte koennen nur ausserhalb
 * von Funktionen, also auf globaler Ebene, verwendet werden. Die
 * damit definierten Namen koennen anschliessend fast genauso wie
 * normale globale Variablen der Sprache C verwendet werden.
 */

/* Benennen eines Special-Function-Registers: */
sfr meinPort = 0x90; /* Hexadezimal 90 ist die Adresse von Port 1 */

/* Benennen des einzelnen Bits 0 dieses Special-Function-Registers: */
sbit meinLsbVonP1 = meinPort^0;
    /* Anmerkung: "^" ist hier *nicht* der bitweise Xor-Operator. */

/* Damit man nicht die Adressen aller Special-Function-Register
 * kennen und immer wieder aufschreiben muss, gibt es eine
 * Include-Datei, die alle Definitionen fuer die Portnamen usw. des
 * Mikrocontrollers C515C enthaelt: */
#include <REG515C.H>
    /* Die include-Operation kann den Dateinamen entweder in "" oder
     * in <> einschliessen. Bei "" wird die Datei zuerst im
     * aktuellen Verzeichnis und dann an einigen Standardplaetzen
     * gesucht. Bei <> geschieht das Gleiche, aber im aktuellen
     * Verzeichnis wird *nicht* gesucht. */

    /* Tipp zum Editor der Entwicklungsumgebung Keil uVision:
     * Mit einem Rechtsklick ins Textfenster bekommt man ein
     * Kontextmenue. Einer der Eintraege dort ist
     * "Insert #include <REG515C.H>". So muss man sich den Namen
     * dieser Datei nicht merken.
     */

/* Einige Variablen: */
unsigned char gelesenesByte;
bit gelesenesBit;

/* Eine Funktion: */
void portIO() {
    /* Einlesen eines Bytes von Port 1: */
    gelesenesByte = P1;
    
    /* Einlesen eines Bits vom niederwertigsten Bit von Port 1: */
    gelesenesBit = meinLsbVonP1;
    /* Nicht erlaubt: "gelesenesBit = P1.0;" oder "gelesenesBit = P1^0;" */
    
    /* Ausgeben eines Bytes auf Port 1: */
    P1 = gelesenesByte;

    /* Ausgeben eines Bits auf das niederwertigste Bit von Port 1: */
    meinLsbVonP1 = 0;

    /* Von der Idee her falsch waere: */
    meinLsbVonP1 = '0'; /* Wirft keinen Fehler und setzt sogar auf 0.*/
        /* Diese Konstante ist ein Buchstabe. Der Dezimalwert davon ist
         * 48. Das unterste Bit davon ist 0. */

    return;
}
