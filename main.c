
// https://www.mikrocontroller.net/articles/Bitmanipulation#Niederwertigstes_gesetztes_Bit_l.C3.B6schen_.28Standard_C.29

#include <REG515C.H>
#include "seriell.h"
#include "bitmanipulation.h"


/** 
 * Vorausdeklaration
 */
int zaehleEinsen(char zeichen);
char setzeParitaet(char zeichen, int anzahlEinsen);
char istGerade(int zahl);
void neueZeile();

 


/** 
 * @function Main-Funktion
 */
main()
{	
	char eingelesenesZeichen, neuesZeichen; // Eingegebene Zeichen, Zeichen nach Paritaetssetzung
	int einsen; // Anzahl an Einsen beim eingegebenen Zeichen
	
	initSeriell();	// Serielle Schnittstelle initialisieren
	
	while(1) // Endlosschleife fuer Programmablauf
	{
		eingelesenesZeichen = readChar();

		einsen = zaehleEinsen(eingelesenesZeichen);
		printChar(einsen +48);	 
		neueZeile();
		
		neuesZeichen = setzeParitaet(eingelesenesZeichen, einsen);
		P5 = neuesZeichen; // In Port 5 laden
	}				
}

/** 
 * @function Zaehlt Anzahl der gesetzten Bits von einem Zeichen
 */
int zaehleEinsen(char zeichen)
{
	int loop, counter, bitindex;
	//bitindex = 1;
	counter = 0;
	
	for ( loop = 0; loop < 7; loop++ ) // Bit 0 bis 6 ueberpruefen
	{		
		
		if ( leseBit(zeichen,loop) == 1 )
		{
			counter++; // dann hochzaehlen
		}	
			
	}	
	
	return counter;
}

/** 
 * @function Setzt bei ungerader Anzahl von gesetzten Bits das 8. Bit als Paritaetsbit
 */
char setzeParitaet(char zeichen, int anzahlEinsen)
{
	if (istGerade(anzahlEinsen) == 1)
	{
		// Das 8. Bit (Bit 7) soll geloescht werden
		//zeichen &= ~(1 << 7);
			zeichen = loescheBit(zeichen, 7);
	}
	else // Ungerade Anzahl
	{
	 
		zeichen = setzeBit(zeichen, 7);
			
	}
	 
	return zeichen;
}

/** 
 * @function Prueft ob Zahl gerade ist
 */
char istGerade(int zahl)
{
	if (zahl % 2 == 1) return 0;	
	else return 1;
}	


void neueZeile()
{
	printChar(10); // Line Feed
	//printChar(9); // Zeilenschub
	//printChar(32); // Space
}
