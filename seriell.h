/** @file seriell.h
 * Eine Bibliothek fuer die Ausgabe und Eingabe eines einzelnen
 * Characters auf der serielle Schnittstelle des
 * C515C-Mikrocontrollers.
 *
 * @author      Jan Bredereke
 * @date        2014, 2020
*/

#ifndef SERIELL_H
#define SERIELL_H

/*
 * --------------------------------------------------------------------------
 * Funktionsdeklarationen
 * --------------------------------------------------------------------------
*/

/** Initialisiert die serielle Schnittstelle.
*/
void initSeriell();

/** Gibt einen einzelnen Character auf die serielle Schnittstelle aus.
 * @attention   Vorher muss initSeriell() aufgerufen worden sein.
*/
void printChar(
    /** Der auszugebende Character. */
    unsigned char character
);

/** Liest einen einzelnen Character von der seriellen Schnittstelle ein.
 * Es wird gewartet, bis ein Character gekommen ist.
 * @attention   Vorher muss initSeriell() aufgerufen worden sein.
*/
unsigned char readChar();

#endif
