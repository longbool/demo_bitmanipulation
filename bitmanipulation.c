
#include "bitmanipulation.h"


int setzeBit(int value, int bitIndex)
{
	value |= (1 << bitIndex);
	return value;
}


int loescheBit(int value, int bitIndex)
{
	value &= ~(1 << bitIndex);
	return value;
}

int leseBit(int value, int bitIndex){
	int bitWert=1;
	int i;
	
	for ( i=0; i<bitIndex; i++ ){
		bitWert *= 2;
	}	

	if (value & bitWert){
		return 1;
	}
	else{
		return 0;	
	}	
	
}