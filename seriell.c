/** @file seriell.c
 * Eine Bibliothek fuer die Ausgabe und Eingabe eines einzelnen
 * Characters auf der serielle Schnittstelle des
 * C515C-Mikrocontrollers.
 *
 * Die Beschreibung der Aufrufschnittstelle befindet sich in der
 * zugehoerigen Header-Datei.
 * @see seriell.h
 *
 * @author      Jan Bredereke
 * @date        2014, 2020
*/

/* Die Deklarationen der Ports des C515C-Mikrocontrollers usw.
*/
#include <REG515C.H>

/* Die Header-Datei mit der Deklaration der Aufruf-Schnittstelle
 * fuer dieses Programm-Modul.
*/
#include "seriell.h"

/*
 * --------------------------------------------------------------------------
 * Funktionsdefinitionen
 * --------------------------------------------------------------------------
*/

void initSeriell() {
    /* Initialisiere die serielle Schnittstelle des C515C
     * auf 9600 Baud, 1 Stoppbit, kein Paritybit.
     * Ein Aufruf ist bei der echten Mikrocontrollerkarte im Labor
     * nicht noetig (aber auch nicht schaedlich), da fuer das Laden des
     * Programms schon eine Initialisierung durch uVision erfolgt.
     * Siehe auch das C515C-User-Manual, Seite 6-48ff.
    */

    /* Lade Baud-Rate-Zaehler, oberer Teil
     * (Formel im User-Manual auf S. 6-54).
    */
    SRELH = 0x03;
    /* Lade Baud-Rate-Zaehler, unterer Teil, und setze Zaehler zurueck. */
    SRELL = 0xbf;
    /* Setze SMOD-Bit im Register PCON (verdoppelt Baud-Rate). */
    PCON |= 0x80;
    /* Setze BD-Bit im Register ADCON0 (Baud rate generator enable). */
    BD = 1;
    /* Setze Bits im Register SCON (von links nach rechts):
     * SM0=0, SM1=1, SM2=0:
     *   serial mode 1, also 8-Bit-UART
     *   mit variabler Baud-Rate,
     *   einem Stoppbit und keinem
     *   Paritybit.
     * REN=1: Receiver enable.
     * TB8=0: unbenutzt (9. Sendebit).
     * RB8=0: unbenutzt (9. Empfangsbit).
     * TI=1: Transmitter-Interrupt-Flag.
     * RI=0: Receiver-Interrupt-Flag.
    */
    SCON = 0x52;

    return;
}

void printChar(
    unsigned char character
) {
    /* Warte ggf., bis das vorige Zeichen fertig ausgegeben ist.
     * (Dann wird das Transmitter-Interrupt-Flag gesetzt.) */
    while (!TI);
    /* Loesche das Transmitter-Interrupt-Flag wieder. */
    TI = 0;
    /* Lege das Zeichen in Sendepuffer und beginne die Uebertragung, ohne
     * auf deren Ende zu Warten. */
    SBUF = character;

    return;
}

unsigned char readChar() {
    /* Warte, bis ein Zeichen empfangen wurde. (Dann wird das
     * Receiver-Interrupt-Flag gesetzt.) */
    while (!RI);
    /* Loesche das Receiver-Interrupt-Flag wieder. */
    RI = 0;
    /* Hole das empfangene Zeichen und gib es zurueck. */
    return SBUF;
}
