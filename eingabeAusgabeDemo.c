/* 
 * Demonstration einfacher Eingabe und Ausgabe auf der seriellen
 * Schnittstelle des Mikrocontrollers C515C in der
 * Programmiersprache C.
 *
 * Die erste Datei dieses Satzes von Dateien ist cGrundlagen.c.
 *
 * Jan Bredereke, 2020
 */

# include "seriell.h"

void eingabeAusgabeDemo() {
    unsigned char taste;

    /* Unsere Mikrocontroller im Labor koennen ueber ihre
     * RS-232-Schnittstelle Texte ausgeben und eingeben.
     *
     * Die Ansteuerung der Peripherie-Hardware im Mikrocontroller
     * ist im Quellcode-Modul seriell.c eingekapselt. Die interne
     * Funktion dieses Moduls ist hier zunaechst nicht relevant. Die
     * Aufrufschnittstelle ist in der Datei seriell.h beschrieben.
     *
     * Im Labor geschieht die Eingabe und Ausgabe im "Serial Window"
     * der Entwicklungsumgebung. Das gilt fuer die Arbeit mit dem
     * echten Mikrocontroller genauso wie fuer die Arbeit mit dem
     * Simulator.
     *
     * Um im "Serial Window" einen Zeilenvorschub zu erhalten, muss
     * man die zwei Zeichen Carriage-Return und Line-Feed ausgeben,
     * also den String "\r\n".
     *
     * Die Hardware wuerde es problemlos zulassen, eine
     * nicht-blockierende Eingabe zu programmieren. Dies ist in
     * seriell.c bisher aber nicht enthalten.
     */

    /* Initialisiert die serielle Schnittstelle: */
    initSeriell();

    /* Liest einen einzelnen Character von der seriellen
     * Schnittstelle ein. Es wird gewartet, bis ein Character
     * gekommen ist.
     *
     * In der Entwicklungsumgebung muss man das "Serial Window"
     * oeffnen und mit der Maus den Fokus hineinsetzen, um einen
     * Tastendruck eingeben zu koennen.
     */
    taste = readChar();

    /* Gibt einen einzelnen Character auf die serielle Schnittstelle
     * aus.
     *
     * In der Entwicklungsumgebung sieht man die Ausgabe im "Serial
     * Window".
     */
    printChar(taste);

    /* Auf Rechnern mit Betriebssystem stehen in der Sprache C viele
     * weitere Funktionen fuer die Eingabe und Ausgabe zur
     * Verfuegung, die in Standardbibliotheken definiert sind. Wir
     * beschreiben sie hier aber nicht.
     *
     * Ein Vorteil von C ist, dass alle diese Funktionen nicht in
     * die Sprache eingebaut sind. Daher kann man auf sehr "kleinen"
     * Systemen einfach die Standardbibliotheken nicht einbinden und
     * ist damit von der Pflicht befreit, diese umfangreiche
     * Funktionalitaet auf diese Plattform zu portieren.
     */

    return;
}
