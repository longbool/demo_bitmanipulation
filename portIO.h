/* 
 * Demonstration der Eingabe und Ausgabe einzelner Bytes und Bits
 * auf Hardware-Ports, fuer die Mikrocontroller-Familie 8051.
 *
 * Diese Header-Datei macht die Funktion(en) und ggf. Variablen aus
 * der zugehoerigen C-Datei fuer andere Dateien bekannt. Dafuer muss
 * sie dort inkludiert werden.
 *
 * Die erste Datei dieses Satzes von Dateien ist cGrundlagen.c.
 *
 * Jan Bredereke, 2020
 */

/* Eine Funktion: */
void portIO();
