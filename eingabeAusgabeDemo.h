/* 
 * Demonstration einfacher Eingabe und Ausgabe auf der seriellen
 * Schnittstelle des Mikrocontrollers C515C in der
 * Programmiersprache C.
 *
 * Diese Header-Datei macht die Funktion(en) und ggf. Variablen aus
 * der zugehoerigen C-Datei fuer andere Dateien bekannt. Dafuer muss
 * sie dort inkludiert werden.
 *
 * Die erste Datei dieses Satzes von Dateien ist cGrundlagen.c.
 *
 * Jan Bredereke, 2020
 */

#ifndef IODEMO_H
#define IODEMO_H


/* Eine Funktion: */
void eingabeAusgabeDemo(void);


#endif